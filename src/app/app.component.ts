import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  slides = [
    {
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      imageName: 'phone-slide.png'
    },
    {
      title: 'Praesent commodo cursus magna, vel scelerisque nisl consectetur.',
      imageName: 'Slide.png'
    }
  ];
}
